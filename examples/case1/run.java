// Simcenter STAR-CCM+ macro: aucMacro.java
// Written by Simcenter STAR-CCM+ 16.03.004
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.cosimulation.link.cgns.*;
import star.cosimulation.link.common.*;
import star.mapping.*;
import star.vis.*;

public class run extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = getActiveSimulation();

    CoSimulation coSimulation_0 = ((CoSimulation) simulation_0.get(CoSimulationManager.class).getObject("Link 1"));

    CgnsImportPartner cgnsImportPartner_0 = coSimulation_0.getCoSimulationValues().get(CgnsImportPartner.class);



    coSimulation_0.executeAction("ImportMesh");
    coSimulation_0.executeAction("ImportFields");

    ((SurfaceDataMapper) simulation_0.get(DataMapperManager.class).getObject("ElevationMapper")).mapData();
    ((SurfaceDataMapper) simulation_0.get(DataMapperManager.class).getObject("VelocityMapper")).mapData();

    simulation_0.getSolution().initializeSolution();

    int loop = 0;
    int maxSteps = 1000;
    while(cgnsImportPartner_0.getStateIndex() < cgnsImportPartner_0.getMaxStateIndex() && loop<maxSteps ) {

        cgnsImportPartner_0.setStateIndex(cgnsImportPartner_0.getStateIndex() + 1);
    
        coSimulation_0.executeAction("ImportFields");
	((SurfaceDataMapper) simulation_0.get(DataMapperManager.class).getObject("ElevationMapper")).mapData();
        ((SurfaceDataMapper) simulation_0.get(DataMapperManager.class).getObject("VelocityMapper")).mapData();

        simulation_0.getSimulationIterator().step(1);
	loop++;
    }
  }
}
