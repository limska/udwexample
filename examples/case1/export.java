// Simcenter STAR-CCM+ macro: export.java
// Written by Simcenter STAR-CCM+ 16.05.007
package macro;

import java.util.*;

import star.common.*;
import star.base.neo.*;
import star.vis.*;
import star.cosimulation.link.common.*;

public class export extends StarMacro {

  public void execute() {
    execute0();
  }

  private void execute0() {

    Simulation simulation_0 = getActiveSimulation();

    simulation_0.getSolution().initializeSolution();

    simulation_0.getTableManager().getTable("elevation").extract();

    CoSimulation coSimulation_0 = ((CoSimulation) simulation_0.get(CoSimulationManager.class).getObject("Link 1"));
    coSimulation_0.executeAction("ExportFile", NeoProperty.fromString("{}"));

    simulation_0.getSimulationIterator().run();

  }
}
