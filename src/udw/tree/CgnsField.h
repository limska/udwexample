#ifndef CgnsField_h_
#define CgnsField_h_

#include <string>
#include <vector>


class CgnsField
{
public:
  std::string _name;
  std::vector<int> _exponents;
};

#endif // CgnsField_h_
