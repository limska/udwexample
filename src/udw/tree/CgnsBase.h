#ifndef CgnsBase_h_
#define CgnsBase_h_

#include "CgnsZone.h"

#include <map>
#include <string>
#include <vector>


class CgnsBase
{
public:
  CgnsBase(std::string const & name, int cellDim, int physicalDim)
    : _name{name}
    , _cellDim{cellDim}
    , _physicalDim{physicalDim}
  { }

  int _id{};
  std::string _name;
  int _cellDim{};
  int _physicalDim{};

  std::map<std::string,std::string> _descriptors;
  std::vector<std::pair<double,int>> _timeIterValues;
  std::vector<CgnsZone> _zones;
};

#endif // CgnsBase_h_
