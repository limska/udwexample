#ifndef CgnsZone_h_
#define CgnsZone_h_

#include "CgnsElements.h"
#include "CgnsGridCoordinates.h"
#include "CgnsSolution.h"

#include <string>
#include <vector>


class CgnsZone
{
public:
  enum class Type {STRUCTURED, UNSTRUCTURED};

  CgnsZone(std::string const & name, Type type)
    : _name{name}
    , _type{type}
  { }

  int _id{};
  std::string _name;
  Type _type;

  CgnsGridCoordinates _gridCoordinates;
  std::vector<CgnsElements> _sections;
  std::vector<CgnsSolution> _solutions;
};

#endif // CgnsZone_h_
