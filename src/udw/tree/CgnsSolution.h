#ifndef CgnsSolution_h_
#define CgnsSolution_h_

#include "CgnsField.h"

#include <string>
#include <vector>


class CgnsSolution
{
public:
  int _id{};
  std::string _name;
  int _location{};
  std::vector<CgnsField> _fields;
};

#endif // CgnsSolution_h_
