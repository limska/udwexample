#ifndef CgnsElements_h_
#define CgnsElements_h_

#include <memory>
#include <string>
#include <vector>


class CgnsElements
{
public:
  CgnsElements(std::string const & name, int type)
    : _name{name}
    , _type{type}
  { }

  int _id{};
  std::string _name;
  int _type{};
  int _startEl{};
  int _numEls{};

  std::vector<int> _connectivity;
};

#endif // CgnsElements_h_
