#ifndef CgnsGridCoordinates_h_
#define CgnsGridCoordinates_h_

#include <array>
#include <vector>
#include <string>


class CgnsGridCoordinates
{
public:
  std::array<std::array<int, 3>, 3> _meshVertexSizes;

  std::vector<double> _x;
  std::vector<double> _y;
  std::vector<double> _z;
};

#endif // CgnsGridCoordinates_h_
