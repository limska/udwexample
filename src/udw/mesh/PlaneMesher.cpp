#include "PlaneMesher.h"

#include "tree/CgnsTree.h"

#include "writer/cgns_wrapper.h"


PlaneMesher::
PlaneMesher(Options const & opts)
  : _opts(opts)
{
}


CgnsTree
PlaneMesher::
mesh(unsigned int orientationMask)
{
  CgnsTree tree;
  tree._bases.push_back(CgnsBase{"Base", 2, 3});

  CgnsBase & base {tree._bases.back()};

  // order of zone generation is important
  if (orientationMask & XY)
  {
    addZoneXY(base, "XY_Plane");
  }
  if (orientationMask & XZ)
  {
    addZoneXZ(base, "XZ_Plane");
  }
  if (orientationMask & YZ)
  {
    addZoneYZ(base, "YZ_Plane");
  }

  return tree;
}

//--------------------------------------------------------------------------------------------------

void
PlaneMesher::
addZoneXY(CgnsBase & base, std::string const & name)
{
  base._zones.push_back(CgnsZone{name, CgnsZone::Type::UNSTRUCTURED});
  CgnsZone & zone {base._zones.back()};

  // setup grid coordinates
  CgnsGridCoordinates & gridCoordinates {zone._gridCoordinates};
  unsigned int const nvx {_opts.nx + 1};
  unsigned int numFaces {_opts.nx * _opts.ny};
  {
    unsigned int const nvy {_opts.ny + 1};
    unsigned int const numVerts {nvx * nvy};
    gridCoordinates._meshVertexSizes[0][0] = numVerts;
    gridCoordinates._meshVertexSizes[0][1] = numFaces;
    gridCoordinates._meshVertexSizes[0][2] = 0;

    double const dx {_opts.lx / _opts.nx};
    double const dy {_opts.ly / _opts.ny};

    gridCoordinates._x.resize(numVerts);
    gridCoordinates._y.resize(numVerts);
    gridCoordinates._z.resize(numVerts);

    for (int j {0}; j < nvy; ++j)
    {
      for (int i {0}; i < nvx; ++i)
      {
        unsigned int const idx {j * nvx + i};
        gridCoordinates._x[idx] = i * dx + _opts.x0;
        gridCoordinates._y[idx] = j * dy + _opts.y0;
        gridCoordinates._z[idx] = _opts.z0;
      }
    }
  }

  // Fill out the face connectivity
  zone._sections.push_back(CgnsElements{name, cgns::QUAD_4});
  CgnsElements & section {zone._sections.back()};
  {
    // see https://cgns.github.io/CGNS_docs_current/sids/conv.html#unst_quad
    section._startEl = 1;
    section._numEls = numFaces;
    section._connectivity.resize(numFaces * 4);

    // loop over faces bottom left vertex
    unsigned int faceNum {0};
    for (int j {0}; j < _opts.ny; ++j)
    {
      for (int i {0}; i < _opts.nx; ++i)
      {
        // note base index for face is based upon vertex spanning, hence using nvx here
        unsigned int const idx {j * nvx + i};
        unsigned int faceOffset {faceNum * 4};
        // note vertex cgns ids are 1 based
        section._connectivity[faceOffset]     = idx + 1;
        section._connectivity[faceOffset + 1] = idx + nvx + 1;
        section._connectivity[faceOffset + 2] = idx + nvx + 2;
        section._connectivity[faceOffset + 3] = idx + 2;
        faceNum++;
      }
    }
  }
}

void
PlaneMesher::
addZoneXZ(CgnsBase & base, std::string const & name)
{
  base._zones.push_back(CgnsZone{name, CgnsZone::Type::UNSTRUCTURED});
  CgnsZone & zone {base._zones.back()};

  // setup grid coordinates
  CgnsGridCoordinates & gridCoordinates {zone._gridCoordinates};
  unsigned int const nvx {_opts.nx + 1};
  unsigned int numFaces {_opts.nx * _opts.nz};
  {
    unsigned int const nvz {_opts.nz + 1};
    unsigned int const numVerts {nvx * nvz};
    gridCoordinates._meshVertexSizes[0][0] = numVerts;
    gridCoordinates._meshVertexSizes[0][1] = numFaces;
    gridCoordinates._meshVertexSizes[0][2] = 0;

    double const dx {_opts.lx / _opts.nx};
    double const dz {_opts.lz / _opts.nz};

    gridCoordinates._x.resize(numVerts);
    gridCoordinates._y.resize(numVerts);
    gridCoordinates._z.resize(numVerts);

    for (int k {0}; k < nvz; ++k)
    {
      for (int i {0}; i < nvx; ++i)
      {
        unsigned int const idx {k * nvx + i};
        gridCoordinates._x[idx] = i * dx + _opts.x0;
        gridCoordinates._y[idx] = _opts.y0;
        gridCoordinates._z[idx] = k * dz + _opts.z0;
      }
    }
  }

  // Fill out the face connectivity
  zone._sections.push_back(CgnsElements{name, cgns::QUAD_4});
  CgnsElements & section {zone._sections.back()};
  {
    // see https://cgns.github.io/CGNS_docs_current/sids/conv.html#unst_quad
    section._startEl = 1;
    section._numEls = numFaces;
    section._connectivity.resize(numFaces * 4);

    // loop over faces bottom left vertex
    unsigned int faceNum {0};
    for (int k {0}; k < _opts.nz; ++k)
    {
      for (int i {0}; i < _opts.nx; ++i)
      {
        // note base index for face is based upon vertex spanning, hence using nvx here
        unsigned int const idx {k * nvx + i};
        unsigned int faceOffset {faceNum * 4};
        // note vertex cgns ids are 1 based
        section._connectivity[faceOffset]     = idx + 1;
        section._connectivity[faceOffset + 1] = idx + 2;
        section._connectivity[faceOffset + 2] = idx + nvx + 2;
        section._connectivity[faceOffset + 3] = idx + nvx + 1;
        faceNum++;
      }
    }
    // e.g. for 5 x 3 vertices
    //
    // 12 13 14 15 16 17  LOOP:
    //  6  7  8  9 10 11      6  7  8  9 10
    //  0  1  2  3  4  5      0  1  2  3  4
  }
}

void
PlaneMesher::
addZoneYZ(CgnsBase & base, std::string const & name)
{
  base._zones.push_back(CgnsZone{name, CgnsZone::Type::UNSTRUCTURED});
  CgnsZone & zone {base._zones.back()};

  // setup grid coordinates
  CgnsGridCoordinates & gridCoordinates {zone._gridCoordinates};
  unsigned int const nvy {_opts.ny + 1};
  unsigned int numFaces {_opts.ny * _opts.nz};
  {
    unsigned int const nvz {_opts.nz + 1};
    unsigned int const numVerts {nvy * nvz};
    gridCoordinates._meshVertexSizes[0][0] = numVerts;
    gridCoordinates._meshVertexSizes[0][1] = numFaces;
    gridCoordinates._meshVertexSizes[0][2] = 0;

    double const dy {_opts.ly / _opts.ny};
    double const dz {_opts.lz / _opts.nz};

    gridCoordinates._x.resize(numVerts);
    gridCoordinates._y.resize(numVerts);
    gridCoordinates._z.resize(numVerts);

    for (int k {0}; k < nvz; ++k)
    {
      for (int j {0}; j < nvy; ++j)
      {
        unsigned int const idx {k * nvy + j};
        gridCoordinates._x[idx] = _opts.x0;
        gridCoordinates._y[idx] = j * dy + _opts.y0;
        gridCoordinates._z[idx] = k * dz + _opts.z0;
      }
    }
  }

  // Fill out the face connectivity
  zone._sections.push_back(CgnsElements{name, cgns::QUAD_4});
  CgnsElements & section {zone._sections.back()};
  {
    // see https://cgns.github.io/CGNS_docs_current/sids/conv.html#unst_quad
    section._startEl = 1;
    section._numEls = numFaces;
    section._connectivity.resize(numFaces * 4);

    // loop over faces bottom left vertex
    unsigned int faceNum {0};
    for (int k {0}; k < _opts.nz; ++k)
    {
      for (int j {0}; j < _opts.ny; ++j)
      {
        // note base index for face is based upon vertex spanning, hence using nvx here
        unsigned int const idx {k * nvy + j};
        unsigned int faceOffset {faceNum * 4};
        // note vertex cgns ids are 1 based
        section._connectivity[faceOffset]     = idx + 1;
        section._connectivity[faceOffset + 1] = idx + nvy + 1;
        section._connectivity[faceOffset + 2] = idx + nvy + 2;
        section._connectivity[faceOffset + 3] = idx + 2;
        faceNum++;
      }
    }
  }
}
