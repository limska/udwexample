#ifndef Solution_h_
#define Solution_h_

#include <map>
#include <vector>

class WaveSolver;

// Field data are created as required in <_data> as the <solve*> functions are called.

class Solution
{
public:
  void solveElevation(WaveSolver const & solver,
                      std::string const & fieldName,
                      std::vector<double> const & x,
                      double const time);

  void solveVelocity(WaveSolver const & solver,
                     std::string const & fieldName,
                     std::vector<double> const & x,
                     std::vector<double> const & z,
                     double const time);

  // field data always gets ordered on names
  std::map<std::string,std::vector<double>> _data;
  std::map<std::string,std::vector<float>> _exponents;
};

#endif // Solution_h_