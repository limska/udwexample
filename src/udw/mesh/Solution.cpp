#include "Solution.h"

#include "wave/WaveSolver.h"

/*
  * CGNS dimensions
  * See https://cgns.github.io/CGNS_docs_current/sids/build.html#DimensionalUnits
  DimensionalUnits_t :=
  {
    MassUnits_t        MassUnits ;                                     (r)
    LengthUnits_t      LengthUnits ;                                   (r)
    TimeUnits_t        TimeUnits ;                                     (r)
    TemperatureUnits_t TemperatureUnits ;                              (r)
    AngleUnits_t       AngleUnits ;                                    (r)

    AdditionalUnits_t :=                                               (o)
    {
      ElectricCurrentUnits_t   ElectricCurrentUnits ;                  (r)
      SubstanceAmountUnits_t   SubstanceAmountUnits ;                  (r)
      LuminousIntensityUnits_t LuminousIntensityUnits ;                (r)
    }
  } ;
*/
namespace
{
  std::vector<float> const LENGTH_UNITS   { 0.0, 1.0, 0.0, 0.0, 0.0,  0.0, 0.0, 0.0 };
  std::vector<float> const VELOCITY_UNITS { 0.0, 1.0,-1.0, 0.0, 0.0,  0.0, 0.0, 0.0 };
}


void
Solution::
solveElevation(WaveSolver const & solver,
               std::string const & fieldName,
               std::vector<double> const & x,
               double const time)
{
  std::size_t const numVertices {x.size()};

  std::vector<double> & elevation {_data[fieldName]};

  elevation.resize(numVertices);

  for (int i {0}; i < numVertices; ++i)
  {
    elevation[i] = solver.solveElevation(x[i], time);
  }
  _exponents[fieldName] = LENGTH_UNITS;
}

void
Solution::
solveVelocity(WaveSolver const & solver,
              std::string const & fieldName,
              std::vector<double> const & x,
              std::vector<double> const & z,
              double const time)
{
  // XZ components only

  std::size_t const numVertices {x.size()};

  std::vector<double> & velocityX {_data[fieldName + "X"]};
  std::vector<double> & velocityZ {_data[fieldName + "Z"]};

  velocityX.resize(numVertices);
  velocityZ.resize(numVertices);

  for (int i {0}; i < numVertices; ++i)
  {
    std::array<double, 3> const velocity {solver.solveVelocity(x[i], z[i], time)};
    velocityX[i] = velocity[0];
    velocityZ[i] = velocity[2];
  }

  _exponents[fieldName + "X"] = VELOCITY_UNITS;
  _exponents[fieldName + "Z"] = VELOCITY_UNITS;
}
