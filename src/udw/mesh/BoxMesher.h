#ifndef BoxMesher_h_
#define BoxMesher_h_

#include "common/Options.h"

class CgnsTree;


class BoxMesher
{
public:
  BoxMesher(Options const & opts);

  CgnsTree mesh();

private:
  Options _opts;
};

#endif // BoxMesher_h_
