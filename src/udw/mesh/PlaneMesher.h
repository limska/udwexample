#ifndef PlaneMesher_h_
#define PlaneMesher_h_

#include "common/Options.h"

class CgnsBase;
class CgnsTree;


class PlaneMesher
{
public:
  // orientation mask bits to define which planes get meshed
  static unsigned int const XY {1};
  static unsigned int const XZ {2};
  static unsigned int const YZ {4};

  PlaneMesher(Options const & opts);

  CgnsTree mesh(unsigned int orientationMask);

private:
  Options _opts;

  void addZoneXY(CgnsBase & base, std::string const & name);
  void addZoneXZ(CgnsBase & base, std::string const & name);
  void addZoneYZ(CgnsBase & base, std::string const & name);
};

#endif // PlaneMesher_h_
