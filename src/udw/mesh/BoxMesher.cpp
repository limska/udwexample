#include "BoxMesher.h"

#include "tree/CgnsTree.h"


BoxMesher::
BoxMesher(Options const & opts)
  : _opts(opts)
{
}


CgnsTree
BoxMesher::
mesh()
{
  CgnsTree tree;
  tree._bases.push_back(CgnsBase{"Base", 3, 3});

  CgnsBase & base {tree._bases.back()};

  base._zones.push_back(CgnsZone{"Box", CgnsZone::Type::STRUCTURED});
  CgnsZone & zone {base._zones.back()};

  // setup grid coordinates
  CgnsGridCoordinates & gridCoordinates {zone._gridCoordinates};

  // vertex size
  unsigned int const nvx {_opts.nx + 1};
  unsigned int const nvy {_opts.ny + 1};
  unsigned int const nvz {_opts.nz + 1};
  gridCoordinates._meshVertexSizes[0][0] = nvx;
  gridCoordinates._meshVertexSizes[0][1] = nvy;
  gridCoordinates._meshVertexSizes[0][2] = nvz;

  // cell size
  gridCoordinates._meshVertexSizes[1][0] = _opts.nx;
  gridCoordinates._meshVertexSizes[1][1] = _opts.ny;
  gridCoordinates._meshVertexSizes[1][2] = _opts.nz;

  // boundary vertex size (always zero for structured grids)
  gridCoordinates._meshVertexSizes[2][0] = 0;
  gridCoordinates._meshVertexSizes[2][1] = 0;
  gridCoordinates._meshVertexSizes[2][2] = 0;

  double const dx {_opts.lx / _opts.nx};
  double const dy {_opts.ly / _opts.ny};
  double const dz {_opts.lz / _opts.nz};

  unsigned int const numVerts {nvx * nvy * nvz};
  gridCoordinates._x.resize(numVerts);
  gridCoordinates._y.resize(numVerts);
  gridCoordinates._z.resize(numVerts);

  for (unsigned int k {0}; k < nvz; ++k)
  {
    for (unsigned int j {0}; j < nvy; ++j)
    {
      for (unsigned int i {0}; i < nvx; ++i)
      {
        unsigned int idx {k * nvy * nvx + j * nvx + i};
        gridCoordinates._x[idx] = i * dx + _opts.x0;
        gridCoordinates._y[idx] = j * dy + _opts.y0;
        gridCoordinates._z[idx] = k * dz + _opts.z0;
      }
    }
  }

  return tree;
}
