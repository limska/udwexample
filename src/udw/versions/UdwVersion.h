#ifndef UdwVersion_h_
#define UdwVersion_h_

#include <string>


class UdwVersion
{
public:
  std::string toString() const;
};

#endif // UdwVersion_h_
