#include "UdwVersion.h"
#include "UdwConfig.h"

#include "cgnslib.h"
#if defined _WIN32 && defined _MSC_VER && defined CG_BUILD_64BIT
  #undef stat
#endif

#include "hdf5.h"
#include "boost/version.hpp"

#include <sstream>


std::string
UdwVersion::
toString() const
{
  unsigned int majHdf5{};
  unsigned int minHdf5{};
  unsigned int relHdf5{};          
  H5get_libversion(&majHdf5, &minHdf5, &relHdf5);

  std::stringstream ss;
  ss
    << "User Defined Waves"
    << " "  << UDW_PRESENTATION
    << " (" << UDW_VER << ")"
    << " "  << DATE
    << "\nUsing Boost " 
    << BOOST_VERSION / 100000 << '.'      // major version
    << BOOST_VERSION / 100 % 1000 << '.'  // minor version
    << BOOST_VERSION % 100                // patch level
    << "\nHDF5 Library:"
    << " "  << majHdf5
    << "."  << minHdf5
    << "."  << relHdf5
    << "\nCGNS Library: "
    << CGNS_DOTVERS;

  return ss.str();
}
