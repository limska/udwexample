#ifndef Exception_h_
#define Exception_h_

#include <exception>
#include <string>

class Exception : public std::exception
{
public:
  Exception();
  Exception(std::string const & description);
  virtual ~Exception() = default;

  virtual char const * what() const throw();

protected:
  void setDescription(std::string const & description);

private:
  std::string _description;
};

#endif // Exception_h_
