#ifndef StringHelper_h_
#define StringHelper_h_

#include <cstring>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <set>
#include <vector>


// Windows
#if defined(_WIN32) || defined(WIN32)

# define UDW_WIN32

#if !defined (STATIC_CCMP_BUILD)
#define LIBRARY_EXPORT __declspec(dllexport)
#define LIBRARY_IMPORT __declspec(dllimport)
#endif

// Note that both UDW_WIN32 and UDW_WIN64 are defined.
// This may turn out to be a bad idea, but it avoids changing every export decl.
#if defined(_WIN64) || defined(WIN64)
#define UDW_WIN64
#endif

// Unix
#else

#define UDW_UNIX

#endif


//! String helper methods
namespace StringHelper 
{
  //! Replace replaceChar with withChar in s.
  void replace(std::string & s, char const replaceChar, char const withChar);

  std::string toLower(std::string const & s);

  std::string toUpper(std::string const & s);

  std::string removeSpaces(std::string s);

  std::string substitute(std::string const & string,
                         std::string const & substring,
                         std::string const & substitution);

  void tokenize(std::string const & string,
                std::string const & separator,
                std::vector<std::string> & output);

  bool compareStringNoCase(std::string const & a, 
                           std::string const & b, bool compare_length = true);

#ifdef UDW_WIN32
# define strcasecmp _stricmp
#endif
  struct CaseInsensitiveCompare
  {
    bool operator()(std::string const & lhs, std::string const & rhs) const
    {
      return strcasecmp(lhs.c_str(), rhs.c_str()) < 0 ? 1 : 0;
    }
  };


  //! Convert a string representation to a value - T must support operator >>
  template<class T>
  bool fromString(std::string const & s, T & t)
  {
    std::istringstream iss(s);
    return !(iss >> t).fail();
  }

  //! Convert x to a string - x must support operator <<
  template<class T>
  std::string toString(T const &x);

  //! Convert x to a string with width and fill character - x must support operator <<
  template<class T>
  std::string toString(T const &x, int width, char fill);

  //! convert a set of T's to a string in format: "[t1, t2, ..., tN]"
  template<class T>
  std::string toString(std::set<T> const &x);

  //! convert a vector of T's to a string in format: "[t1, t2, ..., tN]"
  template<class T>
  std::string toString(std::vector<T> const &x);



  // wstring variants for file names and paths
  void replace(std::wstring & s, wchar_t const replaceChar, wchar_t const withChar);


  std::wstring toLower(std::wstring const & s);

  std::wstring toUpper(std::wstring const & s);

  //! Replace any invalid character sequences and return a valid UTF8 string
  std::string normalize(std::string const & s);

  template < class T > struct hash
  {
    std::size_t operator()(T const & s) const
    {
      unsigned long h {0};
      for (unsigned int i {0}; i<s.size(); ++i)
      {
        h = 5 * h + s[i];
      }
      return std::size_t(h);
    }
  };

} // class StringHelper


template<class T>
std::string 
StringHelper::toString(T const & x)
{
  std::ostringstream convert;
  convert << x;

  return convert.str();
}


template<class T>
std::string 
StringHelper::toString(T const & x, int w, char f)
{
  std::ostringstream convert;
  convert << std::setw(w) << std::setfill(f) << x;

  return convert.str();
}


template <class T>
std::string 
StringHelper::toString(std::set<T> const & list)
{
  std::string listString = "[";

  for (typename std::set<T>::const_iterator it = list.begin(); it != list.end(); ++it)
    {
      if (it != list.begin()) listString += ", ";
      listString += StringHelper::toString(*it);
    }

  listString += "]";
  return listString;
}


template <class T>
std::string 
StringHelper::toString(std::vector<T> const & list)
{
  std::string listString = "[";
  
  for (unsigned int i = 0; i < list.size(); ++i)
    {
      if (i > 0) listString += ", ";
      listString += StringHelper::toString(list.at(i));
    }

  listString += "]";
  return listString;
}

#endif // StringHelper_h_
