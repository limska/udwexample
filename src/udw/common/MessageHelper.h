#ifndef MessageHelper_h_
#define MessageHelper_h_

#include <string>
#include <iosfwd>
#include <sstream>


typedef enum
{
  Info,
  Warning,
  Error
} MessageType;


struct make_string
{
  std::stringstream ss;

  template<typename T>
  make_string & operator << (const T & data)
  {
    ss << data;
    return *this;
  }

  operator std::string()
  {
    return ss.str();
  }
};

void notify(MessageType const msgType, std::string const & prefix, std::string const & msg);

void notifyError(std::string const & msg);

void notifyWarning(std::string const & msg);

void notifyInfo(std::string const & msg);

#endif // MessageHelper_h_
