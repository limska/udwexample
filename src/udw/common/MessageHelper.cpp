#include "common/MessageHelper.h"

#include "common/Exception.h"

#include <iostream>
#include <cstdlib>

void
notify(MessageType const msgType, std::string const & prefix, std::string const & msg)
{
  std::string fullMsg {prefix + msg};
  std::cout << fullMsg << std::endl;
  if (msgType == Error)
  {
    throw Exception(fullMsg);
  }
}


void
notifyError(std::string const & msg)
{
  notify(Error, "Error: ", msg);
}


void
notifyInfo(std::string const & msg)
{
  notify(Info, "Info: ", msg);
}


void
notifyWarning(std::string const & msg)
{
  notify(Warning, "Warning: ", msg);
}

