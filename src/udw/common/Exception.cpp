#include "Exception.h"

Exception::Exception()
  : _description("(undefined exception)")
{}

Exception::Exception(std::string const & description)
: _description(description)
{}


char const *
Exception::
what() const throw()
{
  return _description.c_str();
}


void Exception::setDescription(std::string const & description)
{
  _description = description;
}
