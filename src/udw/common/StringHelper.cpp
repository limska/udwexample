#include "StringHelper.h"

#include <algorithm>
#include <locale>
#include <cstdlib>
#include <cstring>


void
StringHelper::
replace(std::string & s, char const replaceChar, char const withChar)
{
  std::string::size_type const oneChar {1};
  std::string::size_type i {0};

  while (std::string::npos != i)
  {
    i = s.find(replaceChar, i);

    if (std::string::npos != i)
    {
      s.replace(i, oneChar,oneChar, withChar);
      ++i;
    }
  }
}


std::string
StringHelper::
toLower(std::string const & s)
{
  std::locale loc;
  std::string lwr {s};

  unsigned int i{0};
  for (size_t n {lwr.size()}; i != n; ++i)
  {
    lwr[i] = std::tolower(lwr[i], loc);
  }

  return lwr;
}


std::string
StringHelper::
toUpper(std::string const & s)
{
  std::locale loc;
  std::string upr {s};

  unsigned int i{0};
  for (size_t n {upr.size()}; i !=n; ++i)
  {
    upr[i] = std::toupper(upr[i], loc);
  }

  return upr;
}


void
StringHelper::
replace(std::wstring & s, wchar_t const replaceChar, wchar_t const withChar)
{
  std::wstring::size_type const oneChar {1};
  std::wstring::size_type i {0};

  while (std::wstring::npos != i)
  {
    i = s.find(replaceChar, i);

    if (std::wstring::npos != i)
    {
      s.replace(i, oneChar,oneChar, withChar);
      ++i;
    }
  }
}


std::wstring
StringHelper::
toLower(std::wstring const &s)
{
  std::locale loc;
  std::wstring lwr {s};

  unsigned int i{0};
  for (size_t n {lwr.size()}; i != n; ++i)
  {
    lwr[i] = std::tolower(lwr[i], loc);
  }

  return lwr;
}


std::wstring
StringHelper::
toUpper(std::wstring const & s)
{
  std::locale loc;
  std::wstring upr {s};

  unsigned int i {0};
  for (size_t n {upr.size()}; i != n; ++i)
  {
    upr[i] = std::toupper(upr[i], loc);
  }

  return upr;
}


std::string
StringHelper::
removeSpaces(std::string s)
{
  return std::string(s.begin(), std::remove(s.begin(), s.end(), ' '));
}


std::string
StringHelper::
substitute(std::string const & s,
           std::string const & substr,
           std::string const & substitution)
{
  std::string output {s};
  size_t substrlen {substr.length()};
  size_t sublen {substitution.length()};

  size_t it {0};
  size_t pos {output.find(substr,it)};
  while (pos != std::string::npos)
  {
    output.replace(pos, substrlen, substitution);
    it = pos + sublen; // don't search substitution
    pos = output.find(substr, it);
  }
  return output;
}


// Note that the separator is a single string to match, not a
// collection of char separators (eg uses find rather than find_first_of).
// If we need a collection of char seps, maybe we need an overload taking
// array<char> separators.

template<typename T>
void
tokenizeImpl(T const & input,
             T const & separator,
             std::vector<T> & output)
{
  output.clear();
  size_t startPos = 0;
  size_t foundPos = input.find(separator, startPos);
  while (foundPos != std::string::npos)
  {
    if (foundPos - startPos > 0)
    {
      output.push_back(input.substr(startPos, foundPos-startPos));
    }
    startPos = foundPos+separator.size();
    foundPos = input.find(separator, startPos);
  }
  if (startPos < input.size())
  {
    output.push_back(input.substr(startPos));
  }
}


void
StringHelper::
tokenize(std::string const & input,
         std::string const & separator,
         std::vector<std::string> & output)
{
  tokenizeImpl<std::string>(input, separator, output);
}


bool
StringHelper::
compareStringNoCase(std::string const & a, std::string const & b, bool compareLength)
{
  if (compareLength)
  {
    if (a.length() != b.length())
    {
      return false;
    }
  }

  std::string::const_iterator aIt {a.begin()};
  std::string::const_iterator bIt {b.begin()};

  while ((aIt != a.end()) && (bIt != b.end()))
  {
    if (toupper(*aIt) != toupper(*bIt))
    {
      return false;
    }
    ++aIt;
    ++bIt;
  }
  return true;
}


namespace
{
  int const INVALID_CHAR {-1};

  int remaining_bytes(char const * ss)
  {
    unsigned char const c = *ss;
    int nb {0};
    if ((c & 0x80) == 0) nb = 0; // 7 bit ascii
    else if ((c & 0xC0) == 0x80) return INVALID_CHAR;
    else if ((c & 0xE0) == 0xC0) nb = 1;
    else if ((c & 0xF0) == 0xE0) nb = 2;
    else if ((c & 0xF8) == 0xF0) nb = 3;
    else if ((c & 0xFC) == 0xF8) nb = 4;
    else if ((c & 0xFE) == 0xFC) nb = 5;
    else return INVALID_CHAR;

    // verify the remaining bytes of this character are valid continuation bytes
    for (int i {0}; i != nb; ++i)
    {
      if ((*(ss + i + 1) & 0xC0) != 0x80)  // missing continuation marker
      {
        return INVALID_CHAR;
      }
    }
    return nb;
  }
}


// return a copy of s with invalid utf8 characters replaced by ?
std::string
StringHelper::
normalize(std::string const & s)
{
  std::string t;
  t.reserve(s.size());

  char const replacement {'?'};
  char const * ss {s.c_str()};
  bool invalid {false};
  while (*ss)
  {
    int const nbextra {remaining_bytes(ss)};
    if (nbextra == INVALID_CHAR)
    {
      if (!invalid)
      {
        t += replacement;
        invalid = true;
      }
      ++ss;  // skip over the current byte
    }
    else
    {
      t += *(ss++);  // copy the current byte
      for (int i = 0; i != nbextra; ++i, ++ss)
      {
        t += *ss;  // and the extra bytes
      }
      invalid = false;
    }
  }

  return t;
}
