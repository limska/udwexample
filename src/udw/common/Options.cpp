#include "Options.h"

#include <sstream>
#include <string>


std::string
Options::
toString() const
{
  std::stringstream ss;

  ss
    << "Filename: " << filename

    << "\nMesh Type: " << meshType
    
    << "\nLength in X, lx = " << lx
    << "\nLength in Y, ly = " << ly
    << "\nLength in Z, lz = " << lz

    << "\nX coordinate of lower left corner, x0 = " << x0
    << "\nY coordinate of lower left corner, y0 = " << y0
    << "\nZ coordinate of lower left corner, z0 = " << z0

    << "\nNumber of divisions in X, nx = " << nx
    << "\nNumber of divisions in Y, ny = " << ny
    << "\nNumber of divisions in Z, nz = " << nz

    << "\nWave Length, waveLength = " << waveLength
    << "\nWave Amplitude, waveAmplitude = " << waveAmplitude

    << "\nTime step, timeStep = " << timeStep
    << "\nTotal time, totalTime = " << totalTime;
  
  return ss.str();
}
