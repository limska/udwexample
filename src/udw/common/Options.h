#ifndef Options_h_
#define Options_h_

#include <string>


class Options
{
public:
  std::string toString() const;

  std::string filename;
  std::string meshType;

  double lx{};
  double ly{};
  double lz{};

  double x0{};
  double y0{};
  double z0{};

  unsigned int nx{};
  unsigned int ny{};
  unsigned int nz{};

  double waveLength{};
  double waveAmplitude{};

  double timeStep{};
  double totalTime{};
};

#endif // Options_h_
