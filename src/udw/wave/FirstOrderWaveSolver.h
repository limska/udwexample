#ifndef FirstOrderWaveSolver_h_
#define FirstOrderWaveSolver_h_

#include "common/Options.h"

#include "WaveSolver.h"


class FirstOrderWaveSolver : public WaveSolver
{
public:
  FirstOrderWaveSolver(Options const & opts);

  double solveElevation(double const & x, double const & time) const final;
  std::array<double, 3> solveVelocity(double const & x, double const & z, double const & time) const final;

private:
  double _amplitudeValue;
  double _waveLengthValue;
  double const _kappa;
  double const _omega;
};

#endif // FirstOrderWaveSolver_h_
