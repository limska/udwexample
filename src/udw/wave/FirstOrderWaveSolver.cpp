#include "FirstOrderWaveSolver.h"

#include <math.h>

namespace
{
  double const PI {3.14159265358979323846};
}

FirstOrderWaveSolver::
FirstOrderWaveSolver(Options const & opts)
  : _amplitudeValue{opts.waveAmplitude}
  , _waveLengthValue{opts.waveLength}
  , _kappa{2 * PI / _waveLengthValue}
  , _omega{2 * PI / sqrt(2 * PI * _waveLengthValue / 10)}
{}

double
FirstOrderWaveSolver::
solveElevation(double const & x, double const & time) const
{
  double const phase {_kappa * x - _omega * time};
  double const eta {_amplitudeValue * cos(phase)};
  return eta;
}

std::array<double, 3>
FirstOrderWaveSolver::
solveVelocity(double const & x, double const & z, double const & time) const
{
  double const phase {_kappa * x - _omega * time};

  std::array<double, 3> v {0.0, 0.0, 0.0};

  double const eta {_amplitudeValue * cos(phase)};

  if (z - eta <= 0)
  {
    v[0] = _omega * eta * exp(_kappa * z);
    v[2] = _omega * _amplitudeValue * sin(phase) * exp(_kappa * z);
  }

  return v;
}
