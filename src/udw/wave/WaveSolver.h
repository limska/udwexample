#ifndef WaveSolver_h_
#define WaveSolver_h_

#include <array>


class WaveSolver
{
public:
  virtual ~ WaveSolver() = default;

  virtual double solveElevation(double const & x,
                                double const & time) const = 0;

  virtual std::array<double, 3> solveVelocity(double const & x,
                                              double const & z,
                                              double const & time) const = 0;
};

#endif // WaveSolver_h_
