add_library(Options "")
target_link_libraries(Options Common Versions Boost::program_options ${CONAN_LIBS})

target_sources(Options
  PRIVATE
    OptionParser.cpp
  PUBLIC
    OptionParser.h
)