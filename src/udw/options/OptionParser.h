#ifndef OptionParser_h_
#define OptionParser_h_

#include "common/Options.h"

#include <string>
#include <vector>

class OptionParser
{
public:
  OptionParser(int argc, char *argv[]);

  Options getOptions() const;
  std::string argsToString() const;

private:
  std::string _command;
  Options _opts;
  std::vector<std::string> _args;
};

#endif // OptionParser_h_
