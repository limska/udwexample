#include "OptionParser.h"

#include "common/MessageHelper.h"

#include "versions/UdwVersion.h"

#include "boost/program_options.hpp"
#include "common/StringHelper.h"

#include <string>
#include <sstream>


namespace
{
  std::string const VERSION_OPTION {"version"};
  std::string const FULL_VERSION_OPTION {VERSION_OPTION + ",v"};
  std::string const HELP_OPTION {"help"};
  std::string const FULL_HELP_OPTION {HELP_OPTION + ",h"};

  Options makeDefaultOptions()
  {
    Options opts;

    opts.filename = "wave.cgns";
    opts.meshType = "box";
    opts.lx = 10.0;
    opts.ly = 1.0;
    opts.lz = 2.0;
    opts.x0 = -5.0;
    opts.y0 = 0.0;
    opts.z0 = -1.0;
    opts.nx = 5;
    opts.ny = 3;
    opts.nz = 2;
    opts.waveLength = 5;
    opts.waveAmplitude = 0.1;
    opts.timeStep = 0.25;
    opts.totalTime = 0.75;

    return opts;
  }

  void
  getArgumentList(std::vector<boost::program_options::option> const & rawOptions,
                  std::vector<std::string> & args)
  {
    for (boost::program_options::option const & option : rawOptions)
    {
      // Skipping unknown options
      if (!option.unregistered)
      {
        if (option.value.empty())
        {
          args.push_back("--" + option.string_key);
        }
        else
        {
          // this loses order of positional options
          for (std::string const & value : option.value)
          {
            args.push_back("--" + option.string_key);
            args.push_back(value);
          }
        }
      }
    }
  }
}


OptionParser::
OptionParser(int argc, char *argv[])
  : _command(argv[0])
  , _opts{makeDefaultOptions()}
{
  namespace po = boost::program_options;
  po::options_description desc("General Options");

  desc.add_options()
    (FULL_HELP_OPTION.c_str(), "Prints help")
    (FULL_VERSION_OPTION.c_str(), "Prints versions")
    ("file,f", po::value<std::string>(&_opts.filename)->default_value(_opts.filename), "CGNS output file")
    ("mesh,m", po::value<std::string>(&_opts.meshType)->default_value(_opts.meshType), "Mesh type: [box | plane]")
    ("lx", po::value<double>(&_opts.lx)->default_value(_opts.lx), "Length in X direction")
    ("ly", po::value<double>(&_opts.ly)->default_value(_opts.ly), "Length in Y direction")
    ("lz", po::value<double>(&_opts.lz)->default_value(_opts.lz), "Length in Z direction")
    ("x0", po::value<double>(&_opts.x0)->default_value(_opts.x0), "X coordinate of lower left corner")
    ("y0", po::value<double>(&_opts.y0)->default_value(_opts.y0), "Y coordinate of lower left corner")
    ("z0", po::value<double>(&_opts.z0)->default_value(_opts.z0), "Z coordinate of lower left corner")
    ("nx", po::value<unsigned int>(&_opts.nx)->default_value(_opts.nx), "Number of divisions in X direction")
    ("ny", po::value<unsigned int>(&_opts.ny)->default_value(_opts.ny), "Number of divisions in Y direction")
    ("nz", po::value<unsigned int>(&_opts.nz)->default_value(_opts.nz), "Number of divisions in Z direction")
    ("waveLength,l", po::value<double>(&_opts.waveLength)->default_value(_opts.waveLength), "Wave Length")
    ("waveAmplitude,a", po::value<double>(&_opts.waveAmplitude)->default_value(_opts.waveAmplitude), "Wave Amplitude")
    ("deltaT,d", po::value<double>(&_opts.timeStep)->default_value(_opts.timeStep), "Time Step")
    ("time,t",  po::value<double>(&_opts.totalTime)->default_value(_opts.totalTime), "Total Time")
  ;

  po::variables_map vm;
  std::vector<std::string> unrecognized;
  try
  {
    po::parsed_options parsed = po::command_line_parser(argc, argv).options(desc)
                                                                   .allow_unregistered()
                                                                   .run();
    po::store(parsed, vm);

    getArgumentList(parsed.options, _args);

    po::notify(vm);

    unrecognized = po::collect_unrecognized(parsed.options, po::include_positional);
  }
  catch(po::error & e)
  {
    notifyError(std::string{"Options Parse Error: "} + e.what());
  }
  catch(...)
  {
    notifyError(std::string{"Unknown Options Parse Error."});
  }

  if (vm.count(HELP_OPTION))
  {
    notifyInfo(make_string() << desc);
    exit(EXIT_SUCCESS);
  }

  if (vm.count(VERSION_OPTION))
  {
    std::cout << UdwVersion().toString() << std::endl;
    exit(EXIT_SUCCESS);
  }

  if (!unrecognized.empty())
  {
    notifyInfo(make_string() << desc);
    std::string const msg {"Unknown Option: '" + unrecognized[0] + "'"};
    notifyError(msg);
  }
}

Options
OptionParser::
getOptions() const
{
  return _opts;
}

std::string
OptionParser::
argsToString() const
{
  std::stringstream ss;
  for (auto const & arg : _args)
  {
    ss << arg << ' ';
  }
  return ss.str();
}
