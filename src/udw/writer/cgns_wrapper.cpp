#include "cgns_wrapper.h"

#include "cgnslib.h"


#include <filesystem>


namespace cgns
{
  void set_file_type(int file_type)
  {
    call("cg_set_file_type", 
      [&](){ return cg_set_file_type(file_type); });
  }

  void open(std::string const & filename, int *fn)
  {
    if (cg_open(filename.c_str(), CG_MODE_MODIFY, fn) != CG_OK)
    {
      call("cg_open", [&](){ return cg_open(filename.c_str(), CG_MODE_WRITE, fn); });
      call("cg_close", [&](){ return cg_close(*fn); });
    }

    call("cg_open", [&](){ return cg_open(filename.c_str(), CG_MODE_MODIFY, fn); });
  }

  void go_path(int fn, std::string const & path)
  {
    call("cg_gopath", [&](){ 
      return cg_gopath(fn, path.c_str());
    });
  }

  void delete_node(std::string const & name)
  {
    call("cg_delete_node", [&](){ 
      return cg_delete_node(name.c_str());
    });
  }

  int base_write(int fn, std::string const & basename, int cell_dim, int phys_dim)
  {
    int id{};
    call("cg_base_write", [&](){ 
      return cg_base_write(fn, basename.c_str(), cell_dim, phys_dim, &id);
    });
    return id;
  }

  void biter_write(int fn, int B, std::string const & baseIterName, int nSteps)
  {
    call("cg_biter_write", [&](){ 
      return cg_biter_write(fn, B, baseIterName.c_str(), nSteps);
    });
  }

  void dimensional_units_write()
  {
    call("cg_dataclass_write", [&](){
      return cg_dataclass_write(cgns::Dimensional);
    });

    // setup SI units
    call("cg_units_write", [&](){
      return cg_units_write(cgns::Kilogram, cgns::Meter, cgns::Second, cgns::Kelvin, cgns::Degree);
    });
  }

  int zone_write(int fn,
                 int B,
                 std::string const &zonename,
                 std::array<std::array<int, 3>, 3> const & sizes,
                 ZoneType_t zonetype)
  {
    std::vector<cgsize_t> size;
    for (int i=0; i < 3; i++)
    {
      for (int j=0; j < 3; j++)
      {
        size.push_back(sizes[i][j]);
      }
    }

    int id{};
    call("cg_zone_write", [&](){ 
      return cg_zone_write(fn, B, zonename.c_str(), &size[0], zonetype, &id);
    });
    return id;
  }

  void ziter_write(int fn, int B, int Z, std::string const & zoneIterName)
  {
    call("cg_ziter_write", [&](){ 
      return cg_ziter_write(fn, B, Z, zoneIterName.c_str());
    });
  }

  int grid_write(int fn, int B, int Z, std::string const & GridCoordName)
  {
    int id{};
    call("cg_grid_write", [&](){ 
      return cg_grid_write(fn, B, Z, GridCoordName.c_str(), &id);
    });
    return id;
  }

  template<>
  int coord_write(int fn,
                  int B,
                  int Z,
                  std::string const & coordname,
                  std::vector<double> const & coord)
  {
    int id{};
    void const * coord_array = &coord[0];
    call("cg_coord_write", [&](){ 
      return cg_coord_write(fn, B, Z, RealDouble, coordname.c_str(), coord_array, &id); 
    });
    return id;
  }

  int section_write(int fn, int B, int Z, std::string const & sectionName, ElementType_t elType,
                    int nStart, int nEnd, int nBoundaries, std::vector<int> const & connectivity)
  {
    int id{};
    call("cg_section_write", [&](){ 
      return cg_section_write(fn, B, Z, sectionName.c_str(), elType,
                              nStart, nEnd, nBoundaries, connectivity.data(), &id);
    });
    return id;
  }

  int sol_write(int fn, int B, int Z, std::string const & solname, GridLocation_t location)
  {
    int id{};
    call("cg_sol_write", [&](){ 
      return cg_sol_write(fn, B, Z, solname.c_str(), location, &id);
    });
    return id;
  }
  
  template<>
  int field_write(int fn, int B, int Z, int S, std::string const & fieldname,
                  std::vector<double> const & solution_array)
  {
    int id{};
    call("cg_field_write", [&]() {
      return cg_field_write(fn, B, Z, S, RealDouble, fieldname.c_str(), &solution_array[0], &id);
    });
    return id;
  }

  void exponents_write(std::vector<float> const & exponents)
  {
    call("cg_exponents_write", [&]() {
      return cg_exponents_write(RealSingle, &exponents[0]);
    });
  }

  template<>
  void array_write(std::string const & arrayname, int rank, std::vector<double> const & data)
  {
    call("cg_array_write", [&]() {
      cgsize_t dim = data.size();
      return cg_array_write(arrayname.c_str(), RealDouble, rank, &dim, &data[0]);
    });
  }

  template<>
  void array_write(std::string const & arrayname, int rank, std::vector<int> const & data)
  {
    call("cg_array_write", [&]() {
      cgsize_t dim = data.size();
      return cg_array_write(arrayname.c_str(), Integer, rank, &dim, &data[0]);
    });
  }

  void close(int fn)
  {
    call("cg_close", [&](){
      return cg_close(fn);
    });
  }
}

