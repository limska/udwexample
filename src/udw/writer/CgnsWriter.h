#ifndef CgnsWriter_h_
#define CgnsWriter_h_

#include "common/Options.h"

#include <vector>

class CgnsBase;
class CgnsTree;
class Solution;

// This class writes out the CGNS file given the specified CgnsTree structure.
// The file is RAII: opened in constructor, closed in destructor.

class CgnsWriter
{
public:
  CgnsWriter(Options const & opts);
  ~CgnsWriter();

  void write(CgnsTree & tree);
  void write(CgnsBase & base,
             std::vector<Solution> const & zoneSolutions,
             double const time,
             int const iteration);

private:
  Options _opts;
  int _fileIndex;
};

#endif // CgnsWriter_h_
