#ifndef cgns_wrapper_h_
#define cgns_wrapper_h_

#include <iostream>
#include <string>
#include <vector>
#include <array>

#include "common/Exception.h"


namespace cgns
{
  #include "cgnslib.h"

  template<typename F>
  void call(char const * name, F && f)   
  {
    int ier = f();
    if (ier != CG_OK)
    {
      std::cout << "CGNS function " << name << " returned error: " << ier << std::endl;
      throw Exception(cg_get_error());
    }
  }

  template<typename... Args>
  bool node_exists(int fn, int B, Args... args)
  {
    return cg_goto(fn, B, args..., "end") == CG_OK;
  }

  void set_file_type(int file_type);

  void open(std::string const & filename, int *fn);

  template<typename... Args>
  void go_base(int fn, int B, Args... args)
  {
    call("cg_goto", [&](){ 
      return cg_goto(fn, B, args..., "end");
    });
  }
  
  template<typename... Args>
  int go_base_status(int fn, int B, Args... args)
  {
    return cg_goto(fn, B, args..., "end");
  }
  
  template<typename... Args>
  void go_rel(int fn, Args... args)
  {
    call("cg_gorel", [&](){ 
      return cg_gorel(fn, args..., "end");
    });
  }

  void go_path(int fn, std::string const & path);

  void delete_node(std::string const & name);

  int base_write(int fn, std::string const & basename, int cell_dim, int phys_dim);

  void biter_write(int fn, int B, std::string const & baseIterName, int Nsteps);

  void dimensional_units_write();

  int zone_write(int fn,
                  int B,
                  std::string const &zonename,
                  std::array<std::array<int, 3>, 3> const & sizes,
                  ZoneType_t zonetype);

  void ziter_write(int fn, int B, int Z, std::string const & zoneIterName);

  int grid_write(int fn, int B, int Z, std::string const & gridCoordName);

  template<typename T>
  int coord_write(int fn,
                  int B,
                  int Z,
                  std::string const & coordname,
                  std::vector<T> const & coord_array);

  int section_write(int fn, int B, int Z, std::string const & sectionName, ElementType_t elType,
                    int nStart, int nEnd, int nBoundaries, std::vector<int> const & connectivity);

  int sol_write(int fn, int B, int Z, std::string const & solname, GridLocation_t location);

  template<typename T>
  int field_write(int fn, int B, int Z, int S, std::string const & fieldname, 
                  std::vector<T> const & solution_array);

  void exponents_write(std::vector<float> const & exponents);

  template<typename T>
  void array_write(std::string const & arrayname, int rank, std::vector<T> const & data);

  void close(int fn);
}

#endif // cgns_wrapper_h_
