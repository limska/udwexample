#include "CgnsWriter.h"

#include "cgns_wrapper.h"

#include "common/Exception.h"

#include "mesh/Solution.h"

#include "tree/CgnsTree.h"

#include <iostream>
#include <iomanip>
#include <string>

namespace
{
  std::string makeSolutionName(int iteration)
  {
    std::string const numStr {"0000" + std::to_string(iteration)};
    int const SOLUTION_NUM_WIDTH {5};
    return "Solution" + numStr.substr(numStr.length() - SOLUTION_NUM_WIDTH, SOLUTION_NUM_WIDTH);
  }
}


CgnsWriter::
CgnsWriter(Options const & opts)
  : _opts(opts)
{
  std::string const filename {_opts.filename};
  cgns::open(filename, &_fileIndex);
}


CgnsWriter::
~CgnsWriter()
{
  cgns::close(_fileIndex);
}


void
CgnsWriter::
write(CgnsTree & tree)
{
  // writes the CGNS tree structure, updating node ids within the tree.
  for (CgnsBase & base : tree._bases)
  {
    base._id = cgns::base_write(_fileIndex, base._name, base._cellDim, base._physicalDim);

    cgns::go_base(_fileIndex, base._id);
    cgns::dimensional_units_write();

    std::cout
      << "\nSuccessfully wrote base node \"" << base._name
      << "\" id:" << base._id
      << " to file " << _opts.filename
      << std::endl;

    for (CgnsZone & zone : base._zones)
    {
      bool const isStructuredZone {zone._type == CgnsZone::Type::STRUCTURED};

      zone._id = cgns::zone_write(_fileIndex,
                                  base._id,
                                  zone._name, 
                                  zone._gridCoordinates._meshVertexSizes,
                                  isStructuredZone ? cgns::Structured : cgns::Unstructured);

      cgns::coord_write(_fileIndex, base._id, zone._id, "CoordinateX", zone._gridCoordinates._x);
      cgns::coord_write(_fileIndex, base._id, zone._id, "CoordinateY", zone._gridCoordinates._y);
      cgns::coord_write(_fileIndex, base._id, zone._id, "CoordinateZ", zone._gridCoordinates._z);

      std::cout
        << "Successfully wrote " << (isStructuredZone ? "" : "un")
        << "structured grid for zone \"" << zone._name
        << "\" id:" << zone._id
        << " to file " << _opts.filename
        << std::endl;

      // write out any face connectivity sections if unstructured
      if (zone._type == CgnsZone::Type::UNSTRUCTURED)
      {
        for (CgnsElements & section : zone._sections)
        {
          section._id = cgns::section_write(_fileIndex,
                                            base._id,
                                            zone._id,
                                            section._name,
                                            static_cast<cgns::ElementType_t>(section._type),
                                            section._startEl,
                                            section._numEls,
                                            0,
                                            section._connectivity);

          std::cout
            << "Successfully wrote unstructured connectivity \"" << section._name
            << "\" id:" << section._id
            << " to file " << _opts.filename
            << std::endl;
        }
      }
    }
  }
}

void
CgnsWriter::
write(CgnsBase & base,
      std::vector<Solution> const & zoneSolutions,
      double const time,
      int const iteration)
{
  std::string const solutionName {makeSolutionName(iteration)};

  for (int zoneSolutionNum {0}; zoneSolutionNum < zoneSolutions.size(); ++zoneSolutionNum)
  {
    int const zoneId {zoneSolutionNum + 1};
    int const solutionId
    {
      cgns::sol_write(_fileIndex, base._id, zoneId, solutionName, cgns::Vertex)
    };

    for (auto const nameVecPair : zoneSolutions[zoneSolutionNum]._data)
    {
      std::string const & fieldName {nameVecPair.first};
      std::vector<double> const & data {nameVecPair.second};

      int const fieldId
      {
        cgns::field_write(_fileIndex, base._id, zoneId, solutionId, fieldName, data)
      };

      // create and write exponents
      cgns::go_base(_fileIndex, base._id, "Zone_t", zoneId,
                                         "FlowSolution_t", solutionId,
                                         "DataArray_t", fieldId);
      cgns::exponents_write(zoneSolutions[zoneSolutionNum]._exponents.at(fieldName));
    }
  }

  if (cgns::node_exists(_fileIndex, base._id, "BaseIterativeData_t", 1))
  {
    // we replace the old node
    cgns::go_base(_fileIndex, base._id);

    cgns::delete_node("TimeIterValues");
  }

  base._timeIterValues.push_back(std::make_pair(time, iteration));

  std::vector<double> timeValues;
  std::vector<int> iterValues;
  for (auto const & timeIterPair : base._timeIterValues)
  {
    timeValues.push_back(timeIterPair.first);
    iterValues.push_back(timeIterPair.second);
  }

  cgns::biter_write(_fileIndex, base._id, "TimeIterValues", base._timeIterValues.size());

  // write base iterative time values
  cgns::go_base(_fileIndex, base._id, "BaseIterativeData_t", 1);
  cgns::array_write("TimeValues", 1, timeValues);
  // write base iterative iteration values
  cgns::go_base(_fileIndex, base._id, "BaseIterativeData_t", 1);
  cgns::array_write("IterationValues", 1, iterValues);

  std::cout
    << "Successfully wrote solution " 
    << solutionName
    << " at " << std::fixed << std::setprecision(3) << time << '\n'
    << std::endl;
}
