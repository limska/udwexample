#include "UdwApplication.h"

#include "common/Exception.h"
#include "common/Options.h"

#include "options/OptionParser.h"

#include <iostream>
#include <string>


int main (int argc, char *argv[])
{
  std::string error;
  int status {0};
  try
  {
    OptionParser cmdOpts(argc, argv);
    Options const opts {cmdOpts.getOptions()};
    std::cout << "Command line options: " << cmdOpts.argsToString() << std::endl;

    UdwApplication app {opts};
    app.run();
  }
  catch (Exception &e)
  {
    error = e.what();
    status = 1;
  }
  catch (...)
  {
    error = "Unknown error";
    status = 2;
  }

  std::cout
    << "\nudw exit: " << (error.empty() ? "Success." : "Failure.\n")
    << error
    << std::endl;
  
  return status;
}
