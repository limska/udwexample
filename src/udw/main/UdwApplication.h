#ifndef UdwApplication_h_
#define UdwApplication_h_

#include "common/Options.h"


class UdwApplication
{
public:
  UdwApplication(Options const & opts);

  void run();

private:
  Options _opts;

  void writeBoxStructured(std::string const & filename = "");
  void writePlanesUnstructured(std::string const & filename = "");
};

#endif  // UdwApplication_h_