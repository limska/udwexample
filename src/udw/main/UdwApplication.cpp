#include "UdwApplication.h"

#include "mesh/BoxMesher.h"
#include "mesh/PlaneMesher.h"
#include "mesh/Solution.h"

#include "tree/CgnsTree.h"

#include "versions/UdwVersion.h"

#include "writer/CgnsWriter.h"

#include "wave/FirstOrderWaveSolver.h"

#include <iostream>
#include <cmath>


UdwApplication::
UdwApplication(Options const & opts)
  : _opts(opts)
{}


void
UdwApplication::
run()
{
  std::cout
    << UdwVersion().toString()
    << "\n---- UdwApp: Options used:\n"
    << _opts.toString()
    << "\n----"
    << std::endl;

  if (_opts.meshType == "box")
  {
    writeBoxStructured();
  }
  else if (_opts.meshType == "plane")
  {
    writePlanesUnstructured();
  }
}

//--------------------------------------------------------------------------------------------------

void
UdwApplication::
writeBoxStructured(std::string const & filename)
{
  std::cout << "Writing Structured Box Mesh/Solution." << std::endl;

  Options opts {_opts};
  if (!filename.empty())
  {
    // override filename
    opts.filename =  filename;
  }

  //---- Mesh parts structured mesh dx, dy, dz
  BoxMesher boxMesher {opts};
  CgnsTree tree {boxMesher.mesh()};

  // write mesh to file
  CgnsWriter writer{opts};
  writer.write(tree);

  //---- Solve
  int step {0};
  double time {0.0};
  double const timeTolerance {0.001 * opts.timeStep};

  std::cout << "Total time: " << opts.totalTime << std::endl;
  std::cout << "Time Step: " << opts.timeStep << std::endl;

  // only writing one base with one zone for structured mesh
  CgnsBase & base {tree._bases.front()};
  std::vector<Solution> zoneSolutions(1);
  Solution & solution {zoneSolutions[0]};
  CgnsGridCoordinates const & gridCoordinates {base._zones[0]._gridCoordinates};

  FirstOrderWaveSolver const waveSolver {opts};
  while (time < opts.totalTime + timeTolerance)
  {
    std::cout << "Time " << time << std::endl;

    solution.solveElevation(waveSolver, "SurfaceElevation", gridCoordinates._x, time);
    solution.solveVelocity(waveSolver, "Velocity", gridCoordinates._x, gridCoordinates._z, time);

    // write solution to file
    writer.write(base, zoneSolutions, time, step);

    time += opts.timeStep;
    ++step;
  }
}

void
UdwApplication::
writePlanesUnstructured(std::string const & filename)
{
  std::cout << "Writing Unstructured Plane Mesh/Solution." << std::endl;

  Options opts {_opts};
  if (!filename.empty())
  {
    // override filename
    opts.filename =  filename;
  }

  //---- Mesh
  PlaneMesher planeMesher {opts};
  // mesh two 2D planes
  CgnsTree tree {planeMesher.mesh(PlaneMesher::XY | PlaneMesher::XZ)};
  CgnsWriter writer {opts};
  writer.write(tree);

  //---- Solve
  int step {0};
  double time {0.0};
  double const timeTolerance {0.001 * opts.timeStep};

  std::cout << "Total time: " << opts.totalTime << std::endl;
  std::cout << "Time Step: " << opts.timeStep << std::endl;

  CgnsBase & base {tree._bases[0]};
  std::vector<Solution> zoneSolutions(base._zones.size());

  FirstOrderWaveSolver const waveSolver {opts};
  while (time < opts.totalTime + timeTolerance)
  {
    std::cout << "Time " << time << std::endl;

    for (size_t zoneNum{0}; zoneNum < base._zones.size(); ++zoneNum)
    {
      CgnsZone const & zone {base._zones[zoneNum]};
      CgnsGridCoordinates const & gridCoordinates {zone._gridCoordinates};

      // expecting two zones in one base, first zone is XY_Plane, second zone is XZ_Plane
      Solution & solution {zoneSolutions[zoneNum]};
      if (zone._name == "XY_Plane")
      {
        solution.solveElevation(waveSolver, "SurfaceElevation", gridCoordinates._x, time);
      }
      else if (zone._name == "XZ_Plane")
      {
        solution.solveVelocity(waveSolver, "Velocity", gridCoordinates._x, gridCoordinates._z, time);
      }
    }

    // write solutions to file
    writer.write(base, zoneSolutions, time, step);

    time += opts.timeStep;
    ++step;
  }
}
