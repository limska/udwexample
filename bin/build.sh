#!/bin/bash

config=Release
#toolchain=build/conanbuildinfo.cmake

rm -rf build
mkdir build
conan install . -if build -s build_type=${config} --profile default --build outdated

cmake -S. -G "Ninja Multi-Config" -B build \
      -DCMAKE_BUILD_TYPE=${config} \
      -DINCREMENT_VERSION:BOOL=OFF \
       # -DCMAKE_TOOLCHAIN_FILE=${toolchain}
cmake --build build --config ${config}
cmake --install build --config ${config}

(cd build && ctest -C ${config})


