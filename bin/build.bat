REM set config=Debug
set config=Release
REM set toolchain=cmake\toolchains\Windows.cmake
REM set toolchain=..\vcpkg\scripts\buildsystems\vcpkg.cmake
rmdir /q /s build
mkdir build
conan install . -if build -s build_type=%config% --profile default --build missing
cmake -S. -G "Ninja Multi-Config" -B build -DCMAKE_BUILD_TYPE=%config% -DBoost_COMPILER=vc142
cmake --build build --config %config%
cmake --install build --config %config%

cd build
ctest -C %config%
cd ..
