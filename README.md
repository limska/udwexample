# CGNS File Format for User Defined Waves (UDW)

UDW are stored in a CGNS file or a file series. The file needs to contain velocities in the forcing zone and wave height. Logically these 2 fields are stored in a separate CGNS Zone inside a common CGNS Base.  

# Background for User Defined Waves

# Structure of CGNS File

## CGNS Base structure

The following section describes the internal node structure of the CGNS files produced by SC STAR-CCM+ for an unsteady solution type.
The nodes are organised hierarchically as described below:
- CGNSBase_t "Base\<n\>" - root node for a given mesh and zone structure.
  - Descriptor_t "Export Info:" - providing the export time and SC STAR-CCM+ version number.
  - Zone_t "<zone_name>"
    - ZoneType_t "ZoneType" - will be set to "Structured".
    - GridCoordinates_t "GridCoordinates" - single set of mesh vertex coordinates.
    - FlowSolution_t "Solution\<n\>" - solution data sets.
      - GridLocation_t - data for the given field name.
      - DataArray_t "<field_name>" - data for the given field name.
        - DimensionalExponents_t "DimensionalExponents" - the dimension exponents for the field.
    - ZoneIterativeData_t Node
      - DataArray_t "FlowSolutionPointers" - array of solution data set names in order matching BaseIterativeData_t times.
  - DimensionalUnits_t "DimensionalUnits" - contains units of the dimensional exponents reference by the solution data.
  - BaseIterativeData_t "TimeIterValues" - the time and iteration values for the solutions in the base node hierarchy.
    - DataArray_t "TimeValues" - the array of times.
    - DataArray_t "IterationValues" - the array of iterations.

## CGNS schema for UDW solution data

- CGNSBase_t "Volume" - CellDimension = 3, PhysicalDimension = 3
  - Zone_t "VolumeVelocities"
    - ZoneType_t "ZoneType" -> "Structured"
    - GridCoordinates_t "GridCoordinates"
    - ZoneIterativeData_t "ZoneIterativeData"
    - FlowSolution_t "Solution\<n\>"
      - GridLocation_t "GridLocation" -> Cell
  - BaseIterativeData_t "TimeIterValues"
    - DataArray_t "TimeValues"
    - DataArray_t "IterationValues"

- CGNSBase_t "Surface" - CellDimension = 2, PhysicalDimension = 3
  - Zone_t "SurfaceElevation"
    - ZoneType_t "ZoneType" -> "Structured"
    - GridCoordinates_t "GridCoordinates"
    - ZoneIterativeData_t "ZoneIterativeData"
    - FlowSolution_t "Solution\<n\>"
      - GridLocation_t "GridLocation" -> Vertex
  - BaseIterativeData_t "TimeIterValues"
    - DataArray_t "TimeValues"
    - DataArray_t "IterationValues"

Where:
  - Each item gives the CGNS node type (with a trailing "_t") and the name of the node in quotes.
  - Any number spec "\<n\>" is generally a 5 digit number with leading zeroes.
  - <zone_name> will generally correspond to the SC STAR-CCM+ region name.
  - <section_name> corresponds to the SC STAR-CCM+ boundary name.
  - <field_name> corresponds to the name of the exported field function within SC STAR-CCM+.

# Details of HDF5 and CGNS Libraries that are used

- CGNS: 3.4.1
- Boost: 1.75.0

## Build HDF5

## Build CGNS

## Build Boost

## Build UDW on Linux

- git clone git@gitlab.com:limska/udwexample.git
- python3 -m pip install --upgrade pip
- pip3 install conan
- conan profile new default --detect --force
- conan profile update settings.compiler.libcxx=libstdc++17 default
- bin/build.sh

## Build UDW on Windows

- Install Python 3, Conan, CMake, Visual Studio
- git clone git@gitlab.com:limska/udwexample.git
- conan profile new default --detect --force
- conan profile update settings.compiler.libcxx=libstdc++17 default
- bin/build.bat


To be continued...
