include(FetchContent)


set(FETCHCONTENT_QUITE OFF)

FetchContent_Declare(googletest
  GIT_REPOSITORY     https://github.com/google/googletest.git
  GIT_TAG            master
)

set(BOOST_ENABLE_CMAKE ON)

if(MSVC)
  set( BOOTSTRAP "./bootstrap.bat" )
  set( B2 "./b2.exe" )
else(MSVC)
  set( BOOTSTRAP "./bootstrap.sh" )
  set( B2 "./b2" )
endif(MSVC)

FetchContent_Declare(boost
  GIT_REPOSITORY https://github.com/boostorg/boost.git
  GIT_TAG boost-1.75.0
  GIT_PROGRESS 1
  GIT_SUBMODULES
    #tools/boostdep 
    tools/bcp
    tools/build 
    tools/boost_install 
    libs/any 
    libs/array
    libs/assert
    libs/bind
    libs/concept_check
    libs/config 
    libs/container
    libs/container_hash
    libs/core
    libs/date_time
    libs/detail
    libs/function
    libs/headers 
    libs/integer
    libs/intrusive
    libs/iterator
    libs/lexical_cast
    libs/math
    libs/move
    libs/mpl
    libs/numeric/conversion
    libs/predef 
    libs/preprocessor
    libs/program_options 
    libs/range
    libs/smart_ptr
    libs/static_assert
    libs/throw_exception
    libs/tokenizer
    libs/type_index
    libs/type_traits
    libs/utility 
  BUILD_IN_SOURCE 1
  UPDATE_COMMAND ${BOOTSTRAP} 
    --with-libraries=program_options,date_time 
    --prefix=<INSTALL_DIR>
  CONFIGURE_COMMAND ${B2} headers
  BUILD_COMMAND ${B2} install link=static variant=release runtime-link=static
  #CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${boost_LIB_DIR}
  SOURCE_DIR "${boost_SOURCE_DIR}"
  BINARY_DIR "${boost_SOURCE_DIR}"
  INSTALL_COMMAND ""
  #INSTALL_DIR ${boost_INSTALL} 
)

FetchContent_Declare(hdf5
  GIT_REPOSITORY     https://github.com/HDFGroup/hdf5.git
  GIT_TAG            hdf5-1_10_7
)

FetchContent_Declare(cgns
  GIT_REPOSITORY     https://github.com/CGNS/CGNS.git
  GIT_TAG            v3.4.1
)



FetchContent_MakeAvailable(googletest)
