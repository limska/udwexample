macro(make_version)

set(_VER_FILE ${CMAKE_SOURCE_DIR}/VERSION)
file(STRINGS ${_VER_FILE} vars)

include(CMakePrintHelpers)
#cmake_print_variables(vars)

foreach(line ${vars})
  if(line MATCHES "^#")
    continue()
  endif()
  if(line MATCHES "^ *([A-Z_]*) *= *([^#]*) *$")
    set(KEY ${CMAKE_MATCH_1})
    set(VALUE ${CMAKE_MATCH_2})
    set(VERSION_FILE_${KEY} ${VALUE})
  else()
    message(FATAL_ERROR "Line: ${line} not formated correctly should KEY = VALUE")
  endif()
endforeach()

string(REPLACE "." ";" UDW_VER_LIST ${VERSION_FILE_UDW_VER})
list(GET UDW_VER_LIST 0 UDW_MAJOR)
list(GET UDW_VER_LIST 1 UDW_MINOR)
list(GET UDW_VER_LIST 2 UDW_PATCH)
if(${INCREMENT_VERSION})
  math(EXPR UDW_PATCH "${UDW_PATCH} + 1")
endif()
set(VERSION_FILE_UDW_VER "${UDW_MAJOR}.${UDW_MINOR}.${UDW_PATCH}")

string(TIMESTAMP VERSION_FILE_DATE "%Y-%b-%d %H:%M:%S")
set(VERSION_FILE_DATE "\"${VERSION_FILE_DATE}\"")

string(TIMESTAMP VERSION_FILE_UDW_PRESENTATION "%Y.%m")

cmake_print_variables(VERSION_FILE_UDW_VER VERSION_FILE_BOOST_VER VERSION_FILE_UDW_PRESENTATION VERSION_FILE_DATE )

# configure a header file to pass some of the CMake settings
# to the source code
configure_file (
  "${CMAKE_SOURCE_DIR}/src/udw/versions/UdwConfig.h.in"
  "${CMAKE_BINARY_DIR}/UdwConfig.h"
)

configure_file (
  "${CMAKE_SOURCE_DIR}/cmake/VERSION.in"
  "${CMAKE_SOURCE_DIR}/VERSION"
)

endmacro()
