macro(build_tests)

message("")
message("Executing GTest operations:")

configure_file(cmake/gtest/CMakeLists.txt.cmake
               ${CMAKE_BINARY_DIR}/googletest-download/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
                WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/googletest-download )
execute_process(COMMAND ${CMAKE_COMMAND} --build .
                WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/googletest-download )

# Prevent GoogleTest from overriding our compiler/linker options
# when building with Visual Studio
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This adds
# the following targets: gtest, gtest_main, gmock,
# gmock_main and test
add_subdirectory(${CMAKE_BINARY_DIR}/googletest-src
                 ${CMAKE_BINARY_DIR}/googletest-build)

#add_subdirectory(test)
endmacro()


macro(run_tests executable)
if (RunTests)
  set(test "Run${executable}")
  add_custom_target(${test} ALL
                    COMMAND ${executable}
                    DEPENDS ${executable})
endif ()
endmacro()

